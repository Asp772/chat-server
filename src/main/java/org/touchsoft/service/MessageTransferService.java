package org.touchsoft.service;

import org.touchsoft.exception.MessageTransferReceiveException;
import org.touchsoft.exception.MessageTransferSendException;
import org.touchsoft.model.VisitorSocket;
import org.touchsoft.model.action.Action;

public interface MessageTransferService {
    boolean send(VisitorSocket visitorSocket, Action action) throws MessageTransferSendException;

    Action receive(VisitorSocket visitorSocket) throws MessageTransferReceiveException;
}
