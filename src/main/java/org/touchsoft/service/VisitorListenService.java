package org.touchsoft.service;

import org.touchsoft.exception.AcceptVisitorException;
import org.touchsoft.model.VisitorInstance;

public interface VisitorListenService {
    boolean accept(VisitorInstance visitorInstance);

    boolean listen(VisitorInstance visitorInstance);
}
