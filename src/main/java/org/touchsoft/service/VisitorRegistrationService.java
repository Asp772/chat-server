package org.touchsoft.service;

import org.touchsoft.exception.ConversationServiceRunningException;
import org.touchsoft.model.VisitorInstance;

import java.util.concurrent.ExecutorService;

public interface VisitorRegistrationService {
    void addVisitor(VisitorInstance visitorInstance);
}
