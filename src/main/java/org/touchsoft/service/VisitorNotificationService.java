package org.touchsoft.service;

import org.touchsoft.model.VisitorInstance;

public interface VisitorNotificationService {
    boolean notifyInterlocutor(VisitorInstance visitorFrom, VisitorInstance visitorTo);

    boolean greeting(VisitorInstance visitorFrom, VisitorInstance visitorTo);
}
