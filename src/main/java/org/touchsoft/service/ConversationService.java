package org.touchsoft.service;

import org.touchsoft.exception.ConversationServiceRunningException;

import java.util.concurrent.ExecutorService;

public interface ConversationService {
    void createTalking(ExecutorService executorService) throws ConversationServiceRunningException;
}
