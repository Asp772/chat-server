package org.touchsoft.service.impl;

import org.apache.log4j.Logger;
import org.touchsoft.exception.AcceptVisitorException;
import org.touchsoft.exception.ConversationReceiveException;
import org.touchsoft.exception.SocketWrapperCreateException;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.model.VisitorSocket;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.VisitorData;
import org.touchsoft.service.TalkingService;
import org.touchsoft.service.VisitorListenService;
import org.touchsoft.service.VisitorRegistrationService;

public class VisitorListenServiceImpl implements VisitorListenService {
    private final static Logger logger = Logger.getLogger(VisitorListenServiceImpl.class.getName());

    private TalkingService talkingService;
    private VisitorRegistrationService visitorRegistrationService;

    public VisitorListenServiceImpl(TalkingService talkingService, VisitorRegistrationService visitorRegistrationService) {
        this.talkingService = talkingService;
        this.visitorRegistrationService = visitorRegistrationService;

    }


    private boolean acceptVisitor(VisitorInstance visitorInstance) throws AcceptVisitorException {
        boolean isRegistered = false;
        boolean isChoiceMade = false;

        while (!isChoiceMade) {
            try {
                Action visitorAction = this.talkingService.receiveAction(visitorInstance);
                switch (visitorAction.getType()) {
                    case REGISTER: {
                        VisitorData visitorData = (VisitorData) visitorAction.getPayload();
                        visitorInstance.setName(visitorData.getName());
                        visitorInstance.setRole(visitorData.getRole());

//                        VisitorSocket visitorSocketSender = new VisitorSocket(visitorInstance.getSocketWrapperReceiver().getInetAddress().getHostName(), visitorData.getClientPortListened());
//
//                        visitorInstance.setSocketWrapperSender(visitorSocketSender);
                        isRegistered = true;
                        isChoiceMade = true;
                    }
                    break;

                    case LEAVE:
                        isChoiceMade = true;
                        logger.info("user (" + visitorInstance.getVisitorSocket().getLocalAddress() + ") is left");
                        break;

                    default:
                        break;
                }
            } catch (ConversationReceiveException e) {
                throw new AcceptVisitorException(e.getMessage());
            }
//            catch (SocketWrapperCreateException e) {
//                throw new AcceptVisitorException(e.getMessage());
//            }
        }

        return isRegistered;
    }


    @Override
    public boolean accept(VisitorInstance visitorInstance) {
        boolean visitorIsConnected;
        try {
            visitorIsConnected = acceptVisitor(visitorInstance);
            if (visitorIsConnected)
                this.visitorRegistrationService.addVisitor(visitorInstance);

        } catch (AcceptVisitorException e) {
            logger.warn(visitorInstance.getVisitorSocket() + " does not connected");
            visitorIsConnected = false;
        }
        return visitorIsConnected;
    }

    @Override
    public boolean listen(VisitorInstance visitorInstance) {
        boolean visitorIsConnected = true;

        try {
            Action visitorAction = this.talkingService.receiveAction(visitorInstance);

            if (visitorAction.getType() == ActionType.LEAVE) {
                visitorAction.setType(ActionType.FORGET_INTERLOCUTOR);
                visitorIsConnected = false;
                visitorInstance.setConnected(false);
            }

            talkingService.addAction(visitorInstance, visitorAction);
        } catch (ConversationReceiveException e) {
            visitorIsConnected = false;
            visitorInstance.close();
            talkingService.errorOccured(visitorInstance);
        }
        return visitorIsConnected;
    }
}
