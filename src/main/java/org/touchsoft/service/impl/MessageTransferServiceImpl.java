package org.touchsoft.service.impl;

import org.apache.log4j.Logger;
import org.touchsoft.exception.MessageTransferReceiveException;
import org.touchsoft.exception.MessageTransferSendException;
import org.touchsoft.exception.SocketGetOutputStreamException;
import org.touchsoft.exception.SocketGetInputStreamException;
import org.touchsoft.model.VisitorSocket;
import org.touchsoft.service.MessageTransferService;
import org.touchsoft.model.action.Action;

import java.io.*;

public class MessageTransferServiceImpl implements MessageTransferService {
    private final static Logger logger = Logger.getLogger(MessageTransferServiceImpl.class.getName());

    @Override
    public boolean send(VisitorSocket visitorSocket, Action action) throws MessageTransferSendException {
        try {
            ObjectOutputStream objectOutput = new ObjectOutputStream(visitorSocket.getOutputStream());
            objectOutput.writeObject(action);

            return true;
        } catch (SocketGetOutputStreamException e) {
            logger.debug(e.getMessage());
            throw new MessageTransferSendException(e.getMessage());
        } catch (IOException e) {
            logger.debug(e.getMessage());
            throw new MessageTransferSendException(e.getMessage());
        }
    }

    @Override
    public Action receive(VisitorSocket visitorSocket) throws MessageTransferReceiveException {
        try {
            ObjectInputStream objectInput = new ObjectInputStream(visitorSocket.getInputStream());
            Object object = objectInput.readObject();

            return (Action) object;
        } catch (SocketGetInputStreamException e) {
            logger.debug(e.getMessage());
            throw new MessageTransferReceiveException(e.getMessage());
        } catch (IOException e) {
            logger.debug(e.getMessage());
            throw new MessageTransferReceiveException(e.getMessage());
        } catch (ClassNotFoundException e) {
            logger.debug(e.getMessage());
            throw new MessageTransferReceiveException(e.getMessage());
        }
    }
}
