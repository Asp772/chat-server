package org.touchsoft.service.impl;

import org.apache.log4j.Logger;
import org.touchsoft.exception.ConversationSendException;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.service.TalkingService;
import org.touchsoft.service.VisitorNotificationService;
import org.touchsoft.service.VisitorRegistrationService;

public class VisitorNotificationServiceImpl implements VisitorNotificationService {
    private final static Logger logger = Logger.getLogger(VisitorNotificationServiceImpl.class.getName());

    private TalkingService talkingService;
    private VisitorRegistrationService visitorRegistrationService;

    public VisitorNotificationServiceImpl(TalkingService talkingService, VisitorRegistrationService visitorRegistrationService) {
        this.talkingService = talkingService;
        this.visitorRegistrationService = visitorRegistrationService;
    }

    @Override
    public boolean notifyInterlocutor(VisitorInstance visitorFrom, VisitorInstance visitorTo) {
        boolean acceptMessages = true;
        try {
            Action visitorAction = this.talkingService.getAction(visitorFrom);
            if (visitorAction != null) {
                switch (visitorAction.getType()) {
                    case FORGET_INTERLOCUTOR: {
                        this.talkingService.sendAction(visitorTo, visitorAction);

                        visitorAction.setType(ActionType.FORGOTTEN);
                        this.talkingService.addAction(visitorTo, visitorAction);
                        acceptMessages = false;
                    }
                    break;

                    case FORGOTTEN: {
                        logger.info(visitorTo.getName() + " was forgotten");

                        this.visitorRegistrationService.addVisitor(visitorFrom);
                        visitorTo.close();
                        acceptMessages = false;
                    }
                    break;

                    case INTERLOCUTOR_BROKEN: {
                        logger.warn(visitorFrom.getName() + " was broken");
                        visitorAction.setType(ActionType.SEND_MESSAGE);
                        this.talkingService.sendAction(visitorTo, visitorAction);
                        this.visitorRegistrationService.addVisitor(visitorTo);
                        acceptMessages = false;
                    }
                    break;

                    case SEND_MESSAGE: {
                        this.talkingService.sendAction(visitorTo, visitorAction);
                    }
                    break;
                }
            }
            return acceptMessages;
        } catch (ConversationSendException e) {
            logger.warn(e.getMessage());
            visitorTo.close();
            this.visitorRegistrationService.addVisitor(visitorFrom);
        }
        return false;
    }

    @Override
    public boolean greeting(VisitorInstance visitorFrom, VisitorInstance visitorTo) {
        try {
            this.talkingService.sendAction(visitorTo, new Action(ActionType.SEND_MESSAGE,
                    new MessagePayload(visitorFrom.getRole() + " " + visitorFrom.getName() + " connected")));
            return true;
        } catch (ConversationSendException e) {
            logger.warn(e.getMessage());
            visitorTo.close();
            this.visitorRegistrationService.addVisitor(visitorFrom);
        }
        return false;
    }
}
