package org.touchsoft.service.impl;

import org.touchsoft.exception.ConversationServiceRunningException;
import org.touchsoft.manager.VisitorNotificationManager;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.service.VisitorNotificationService;
import org.touchsoft.service.ConversationService;
import org.touchsoft.storage.AwailableVisitorStorage;

import java.util.concurrent.ExecutorService;

public class ConversationServiceImpl implements ConversationService {
    private VisitorNotificationService visitorNotificationService;

    public ConversationServiceImpl(VisitorNotificationService visitorNotificationService) {
        this.visitorNotificationService = visitorNotificationService;
    }

    private boolean isCreateConversationPossible() {
        return !(AwailableVisitorStorage.getInstance().getAgentDeque().isEmpty() || AwailableVisitorStorage.getInstance().getClientDeque().isEmpty());
    }

    @Override
    public void createTalking(ExecutorService executorService) throws ConversationServiceRunningException {
        boolean isConversationPossible;

        synchronized (AwailableVisitorStorage.getInstance()) {
            if (isCreateConversationPossible()) {

                VisitorInstance client = AwailableVisitorStorage.getInstance().getClientDeque().poll();
                VisitorInstance agent = AwailableVisitorStorage.getInstance().getAgentDeque().poll();

                isConversationPossible = client.isConnected() && agent.isConnected();

                if (isConversationPossible) {
                    agent.getVisitorActions().clear();

                    executorService.execute(new VisitorNotificationManager(client, agent, visitorNotificationService));
                    executorService.execute(new VisitorNotificationManager(agent, client, visitorNotificationService));
                } else {
                    if (client.isConnected())
                        AwailableVisitorStorage.getInstance().getClientDeque().addFirst(client);
                    if (agent.isConnected())
                        AwailableVisitorStorage.getInstance().getAgentDeque().addFirst(agent);
                }

            } else {
                try {
                    AwailableVisitorStorage.getInstance().wait();
                } catch (InterruptedException e) {
                    throw new ConversationServiceRunningException(e.getMessage());
                }
            }
        }
    }
}
