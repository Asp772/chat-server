package org.touchsoft.service.impl;

import org.touchsoft.model.VisitorInstance;
import org.touchsoft.service.VisitorRegistrationService;
import org.touchsoft.storage.AwailableVisitorStorage;


public class VisitorRegistrationServiceImpl implements VisitorRegistrationService {
    @Override
    public void addVisitor(VisitorInstance visitorInstance) {
        synchronized (AwailableVisitorStorage.getInstance()) {
            switch (visitorInstance.getRole()) {
                case ROLE_AGENT:
                    AwailableVisitorStorage.getInstance().getAgentDeque().add(visitorInstance);
                    break;
                case ROLE_CLIENT:
                    AwailableVisitorStorage.getInstance().getClientDeque().add(visitorInstance);
                    break;
            }
            AwailableVisitorStorage.getInstance().notify();
        }
    }

}
