package org.touchsoft.service.impl;

import org.apache.log4j.Logger;
import org.touchsoft.exception.*;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.service.TalkingService;
import org.touchsoft.service.MessageTransferService;

public class TalkingServiceImpl implements TalkingService {
    private final String BROKEN_CONNECTION_MESSAGE = "Notification:interlocutor was broken";

    private final static Logger logger = Logger.getLogger(TalkingServiceImpl.class.getName());

    private MessageTransferService messageTransfer;
    private Action BROKEN_INTERLOCUTOR;
    public TalkingServiceImpl(MessageTransferService messageTransfer) {
        this.messageTransfer = messageTransfer;
        BROKEN_INTERLOCUTOR = new Action(ActionType.INTERLOCUTOR_BROKEN, new MessagePayload(BROKEN_CONNECTION_MESSAGE));
    }

    @Override
    public void addAction(VisitorInstance visitorInstance, Action action) {
        visitorInstance.getVisitorActions().add(action);
    }

    @Override
    public void errorOccured(VisitorInstance visitorInstance) {
        visitorInstance.getVisitorActions().add(BROKEN_INTERLOCUTOR);
    }

    @Override
    public Action getAction(VisitorInstance visitorInstance) {
        return visitorInstance.getVisitorActions().poll();
    }

    @Override
    public void sendAction(VisitorInstance clientInstance, Action action) throws ConversationSendException {
        try {
            this.messageTransfer.send(clientInstance.getVisitorSocket(), action);
        } catch (MessageTransferSendException e) {
            logger.debug(e.getMessage());
            throw new ConversationSendException(e.getMessage());
        }
    }

    @Override
    public Action receiveAction(VisitorInstance clientInstance) throws ConversationReceiveException {
        try {
            return this.messageTransfer.receive(clientInstance.getVisitorSocket());
        } catch (MessageTransferReceiveException e) {
            logger.debug(e.getMessage());
            throw new ConversationReceiveException(e.getMessage());
        }
    }

}
