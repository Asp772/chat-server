package org.touchsoft.service;

import org.touchsoft.exception.ConversationReceiveException;
import org.touchsoft.exception.ConversationSendException;
import org.touchsoft.exception.ConversationServiceRunningException;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.model.action.Action;

import java.util.concurrent.ExecutorService;

public interface TalkingService {

    void addAction(VisitorInstance visitorInstance, Action action);

    void errorOccured(VisitorInstance visitorInstance);

    Action getAction(VisitorInstance clientInstance);

    void sendAction(VisitorInstance clientInstance, Action action) throws ConversationSendException;

    Action receiveAction(VisitorInstance clientInstance) throws ConversationReceiveException;
}
