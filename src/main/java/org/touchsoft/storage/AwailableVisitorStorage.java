package org.touchsoft.storage;

import org.touchsoft.model.VisitorInstance;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

public class AwailableVisitorStorage {
    private static volatile AwailableVisitorStorage instance;

    private Deque<VisitorInstance> clientDeque;
    private Deque<VisitorInstance> agentDeque;

    public static AwailableVisitorStorage getInstance() {
        AwailableVisitorStorage localInstance = instance;
        if (localInstance == null) {
            synchronized (AwailableVisitorStorage.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new AwailableVisitorStorage();
                }
            }
        }
        return localInstance;
    }

    private AwailableVisitorStorage() {
        this.clientDeque = new ConcurrentLinkedDeque<>();
        this.agentDeque = new ConcurrentLinkedDeque<>();
    }

    public Deque<VisitorInstance> getClientDeque() {
        return clientDeque;
    }

    public Deque<VisitorInstance> getAgentDeque() {
        return agentDeque;
    }
}
