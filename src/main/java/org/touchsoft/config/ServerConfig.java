package org.touchsoft.config;

import java.util.ResourceBundle;

public class ServerConfig {
    private static final String PROPERTIES_FILE="application";
    private static final String PORT_NAME="PORT";
    private static final String THREAD_NUMBER_NAME="THREAD-NUMBER";

    public static int PORT;
    public static int THREAD_NUMBER;

    static{
        ResourceBundle resourceBundle= ResourceBundle.getBundle(PROPERTIES_FILE);
        PORT=Integer.parseInt(resourceBundle.getString(PORT_NAME));
        THREAD_NUMBER=Integer.parseInt(resourceBundle.getString(THREAD_NUMBER_NAME));
    }
}