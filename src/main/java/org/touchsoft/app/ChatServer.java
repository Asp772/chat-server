package org.touchsoft.app;

import org.apache.log4j.Logger;
import org.touchsoft.ChatServerLauncher;
import org.touchsoft.exception.AcceptVisitorException;
import org.touchsoft.exception.AccepterInitiationException;
import org.touchsoft.exception.ChatServerException;
import org.touchsoft.exception.ServerLaunchException;
import org.touchsoft.manager.VisitorListenManager;
import org.touchsoft.manager.VisitorAssociationManager;
import org.touchsoft.model.VisitorAccepter;
import org.touchsoft.model.VisitorSocket;
import org.touchsoft.service.VisitorListenService;
import org.touchsoft.service.ConversationService;

import java.util.concurrent.ExecutorService;

public class ChatServer {
    final static Logger logger = Logger.getLogger(ChatServerLauncher.class.getName());

    private int port;
    private ExecutorService listenerExecutorService;
    private ExecutorService senderExecutorService;
    private VisitorListenService visitorListenService;
    private ConversationService conversationService;

    public ChatServer(int port, ConversationService conversationService,
                      ExecutorService listenerExecutorService, ExecutorService senderExecutorService,
                      VisitorListenService visitorListenService) {
        this.port = port;
        this.visitorListenService = visitorListenService;
        this.listenerExecutorService = listenerExecutorService;
        this.senderExecutorService = senderExecutorService;
        this.conversationService = conversationService;
    }

    public void launch() throws ChatServerException {
        boolean isAcceptable = true;

        try {
            VisitorAccepter visitorAccepter;
            try {
                visitorAccepter = new VisitorAccepter(this.port);
            } catch (AccepterInitiationException e) {
                throw new ServerLaunchException(e.getMessage());
            }

            logger.info("Server started on port: " + this.port);

            VisitorAssociationManager visitorAssociationManager = new VisitorAssociationManager(this.conversationService,
                    this.senderExecutorService);
            visitorAssociationManager.start();

            while (isAcceptable) {
                handleVisitor(visitorAccepter);
            }
        } catch (ServerLaunchException e) {
            throw new ChatServerException(e.getMessage());
        }
    }

    private void handleVisitor(VisitorAccepter visitorAccepter) {

        VisitorSocket visitorSocket;
        try {
            visitorSocket = visitorAccepter.acceptVisitor();

            logger.info(visitorSocket.getInetAddress() + " tries to connect");

            this.listenerExecutorService.execute(new VisitorListenManager(visitorSocket, this.visitorListenService));
        } catch (AcceptVisitorException e) {
            logger.warn(e.getMessage());
        }
    }
}
