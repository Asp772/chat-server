package org.touchsoft.model;

import org.touchsoft.exception.SocketWrapperCreateException;
import org.touchsoft.exception.SocketGetOutputStreamException;
import org.touchsoft.exception.SocketWrapperCloseException;
import org.touchsoft.exception.SocketGetInputStreamException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class VisitorSocket {
    private Socket socket;

    public VisitorSocket(Socket socket) {
        this.socket = socket;
    }

    public VisitorSocket(String host, int port) throws SocketWrapperCreateException {
        try {
            this.socket = new Socket(host, port);
        } catch (IOException e) {
            throw new SocketWrapperCreateException(e.getMessage());
        }
    }

    public InputStream getInputStream() throws SocketGetInputStreamException {
        try {
            return this.socket.getInputStream();
        } catch (IOException e) {
            throw new SocketGetInputStreamException(e.getMessage());
        }
    }

    public OutputStream getOutputStream() throws SocketGetOutputStreamException {
        try {
            return this.socket.getOutputStream();
        } catch (IOException e) {
            throw new SocketGetOutputStreamException(e.getMessage());
        }
    }

    public void closeSocket() throws SocketWrapperCloseException {
        try {
            this.socket.close();
        } catch (IOException e) {
            throw new SocketWrapperCloseException(e.getMessage());
        }
    }

    public InetAddress getLocalAddress() {
        return this.socket.getLocalAddress();
    }

    public InetAddress getInetAddress() {
        return this.socket.getInetAddress();
    }
}
