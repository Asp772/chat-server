package org.touchsoft.model.action;

import org.touchsoft.model.action.payload.Payload;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Acer on 003 03.11.18.
 */
public class Action implements Serializable {
    private static final long serialVersionUID = -9032700116707333283L;

    private ActionType type;
    private Payload payload;

    public Action(ActionType type, Payload payload) {
        this.type = type;
        this.payload = payload;
    }

    public ActionType getType() {
        return type;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Action action = (Action) o;
        return type == action.type &&
                Objects.equals(payload, action.payload);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, payload);
    }
}
