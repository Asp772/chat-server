package org.touchsoft.model.action;

import java.io.Serializable;

/**
 * Created by Acer on 003 03.11.18.
 */
public enum ActionType implements Serializable {
    SEND_MESSAGE, LEAVE, REGISTER, FORGET_INTERLOCUTOR, FORGOTTEN, INTERLOCUTOR_BROKEN;
}
