package org.touchsoft.model;

import org.touchsoft.exception.AcceptVisitorException;
import org.touchsoft.exception.AccepterInitiationException;

import java.io.IOException;
import java.net.ServerSocket;

public class VisitorAccepter {
    private ServerSocket serverSocket;

    public VisitorAccepter(int port) throws AccepterInitiationException {
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new AccepterInitiationException(e.getMessage());
        }
    }

    public VisitorSocket acceptVisitor() throws AcceptVisitorException {
        try {
            return new VisitorSocket(this.serverSocket.accept());
        } catch (IOException e) {
            throw new AcceptVisitorException(e.getMessage());
        }
    }
}
