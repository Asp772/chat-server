package org.touchsoft.model;

import org.touchsoft.exception.SocketWrapperCloseException;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.payload.impl.Role;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class VisitorInstance {
    private VisitorSocket visitorSocket;
//    private VisitorSocket visitorSocketSender;
    private BlockingQueue<Action> visitorActions;
    private String name;
    private Role role;
    private AtomicBoolean isConnected;

    public boolean isConnected() {
        return isConnected.get();
    }

    public void setConnected(boolean connected) {
        isConnected.set(connected);
    }

    public VisitorInstance() {
        isConnected = new AtomicBoolean(true);
    }

    public BlockingQueue<Action> getVisitorActions() {
        return visitorActions;
    }

    public void setVisitorActions(BlockingQueue<Action> visitorActions) {
        this.visitorActions = visitorActions;
    }

    public VisitorSocket getVisitorSocket() {
        return visitorSocket;
    }

    public void setVisitorSocket(VisitorSocket visitorSocket) {
        this.visitorSocket = visitorSocket;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void close() {
        isConnected.set(false);
        try {
            visitorSocket.closeSocket();
        } catch (SocketWrapperCloseException e) {
            System.err.println(e.getMessage());
        }
    }
}
