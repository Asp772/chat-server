package org.touchsoft.manager;

import org.touchsoft.model.VisitorSocket;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.service.VisitorListenService;

import java.util.concurrent.ArrayBlockingQueue;

public class VisitorListenManager extends Thread {
    private VisitorInstance visitorInstance;
    private VisitorListenService visitorListenService;

    public VisitorListenManager(VisitorSocket visitorSocket, VisitorListenService visitorListenService) {
        this.visitorListenService = visitorListenService;

        this.visitorInstance = new VisitorInstance();
        this.visitorInstance.setVisitorSocket(visitorSocket);
        this.visitorInstance.setVisitorActions(new ArrayBlockingQueue<>(100, true));
    }

    public void run() {
        boolean visitorIsConnected = this.visitorListenService.accept(this.visitorInstance);
        while (visitorIsConnected) {
            visitorIsConnected = this.visitorListenService.listen(this.visitorInstance);
        }
    }
}


