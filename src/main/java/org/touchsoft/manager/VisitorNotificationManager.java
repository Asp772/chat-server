package org.touchsoft.manager;

import org.apache.log4j.Logger;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.service.VisitorNotificationService;

public class VisitorNotificationManager extends Thread {
    private final static Logger logger = Logger.getLogger(VisitorNotificationManager.class.getName());

    private VisitorInstance visitorFrom;
    private VisitorInstance visitorTo;
    private VisitorNotificationService visitorNotificationService;

    public VisitorNotificationManager(VisitorInstance visitorFrom, VisitorInstance visitorTo,
                                      VisitorNotificationService visitorNotificationService) {
        this.visitorFrom = visitorFrom;
        this.visitorTo = visitorTo;
        this.visitorNotificationService = visitorNotificationService;
    }

    public void run() {
        boolean acceptMessages;
        acceptMessages = this.visitorNotificationService.greeting(visitorFrom, visitorTo);
        while (acceptMessages) {
            acceptMessages = this.visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo);
        }
    }
}
