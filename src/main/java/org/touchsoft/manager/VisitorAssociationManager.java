package org.touchsoft.manager;

import org.apache.log4j.Logger;
import org.touchsoft.exception.ConversationServiceRunningException;
import org.touchsoft.service.ConversationService;

import java.util.concurrent.ExecutorService;

public class VisitorAssociationManager extends Thread {
    private final static Logger logger = Logger.getLogger(VisitorAssociationManager.class.getName());

    private boolean isAlive;
    private ConversationService conversationService;
    private ExecutorService executorService;

    public VisitorAssociationManager(ConversationService conversationService, ExecutorService executorService) {
        this.isAlive = true;
        this.conversationService = conversationService;
        this.executorService = executorService;
    }

    public void run() {
        while (this.isAlive) {
            try {
                this.conversationService.createTalking(this.executorService);
            } catch (ConversationServiceRunningException e) {
                logger.error(e.getMessage());
                this.isAlive = false;
            }
        }
    }
}

