package org.touchsoft;

import org.apache.log4j.Logger;
import org.touchsoft.app.ChatServer;
import org.touchsoft.config.ServerConfig;
import org.touchsoft.exception.ChatServerException;
import org.touchsoft.service.*;
import org.touchsoft.service.impl.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServerLauncher {
    private final static Logger logger = Logger.getLogger(ChatServerLauncher.class.getName());

    public static void main(String[] args) {
        ExecutorService listenerExecutorService = Executors.newFixedThreadPool(ServerConfig.THREAD_NUMBER);
        ExecutorService notificatorExecutorService = Executors.newCachedThreadPool();
        try {
            logger.info("Trying to start server");

            MessageTransferService messageTransfer = new MessageTransferServiceImpl();
            TalkingService talkingService = new TalkingServiceImpl(messageTransfer);
            VisitorRegistrationService visitorRegistrationService = new VisitorRegistrationServiceImpl();
            VisitorNotificationService visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
            VisitorListenService visitorListenService = new VisitorListenServiceImpl(talkingService, visitorRegistrationService);
            ConversationService conversationService = new ConversationServiceImpl(visitorNotificationService);

            ChatServer chatServer = new ChatServer(ServerConfig.PORT, conversationService, listenerExecutorService,
                    notificatorExecutorService, visitorListenService);
            chatServer.launch();

        } catch (ChatServerException e) {
            logger.error(e.getMessage(), e.getCause());
        }
    }
}
