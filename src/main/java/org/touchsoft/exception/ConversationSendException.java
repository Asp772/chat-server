package org.touchsoft.exception;

import java.io.IOException;

public class ConversationSendException extends IOException {
    public ConversationSendException(String message) {
        super(message);
    }
}
