package org.touchsoft.exception;

import java.io.IOException;

public class AccepterInitiationException extends IOException {
    public AccepterInitiationException(String message) {
        super(message);
    }
}
