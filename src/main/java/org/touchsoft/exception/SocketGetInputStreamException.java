package org.touchsoft.exception;

import java.io.IOException;

public class SocketGetInputStreamException extends IOException {
    public SocketGetInputStreamException(String message) {
        super(message);
    }
}
