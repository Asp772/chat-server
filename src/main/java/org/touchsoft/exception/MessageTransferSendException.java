package org.touchsoft.exception;

import java.io.IOException;

public class MessageTransferSendException extends IOException {
    public MessageTransferSendException(String message){super(message);}
}
