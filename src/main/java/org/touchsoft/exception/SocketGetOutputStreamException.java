package org.touchsoft.exception;

import java.io.IOException;

public class SocketGetOutputStreamException extends IOException {
    public SocketGetOutputStreamException(String message) {
        super(message);
    }
}
