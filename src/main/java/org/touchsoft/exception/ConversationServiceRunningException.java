package org.touchsoft.exception;

import java.io.IOException;

public class ConversationServiceRunningException extends IOException {
    public ConversationServiceRunningException(String message) {
        super(message);
    }
}
