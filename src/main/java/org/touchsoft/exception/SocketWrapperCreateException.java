package org.touchsoft.exception;

import java.io.IOException;

public class SocketWrapperCreateException extends IOException {
    public SocketWrapperCreateException(String message) {
        super(message);
    }
}
