package org.touchsoft.exception;

import java.io.IOException;

public class SocketWrapperCloseException extends IOException {
    public SocketWrapperCloseException(String message) {
        super(message);
    }
}
