package org.touchsoft.exception;

public class ServerLaunchException extends Throwable {
    public ServerLaunchException(String message) {
        super(message);
    }
}
