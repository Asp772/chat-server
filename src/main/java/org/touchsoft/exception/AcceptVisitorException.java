package org.touchsoft.exception;

import java.io.IOException;

public class AcceptVisitorException extends IOException{
    public AcceptVisitorException(String message) {
        super(message);
    }
}
