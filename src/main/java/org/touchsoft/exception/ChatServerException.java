package org.touchsoft.exception;

public class ChatServerException extends Throwable {
    public ChatServerException(String message) {
        super(message);
    }

}
