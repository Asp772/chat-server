package org.touchsoft.exception;

import java.io.IOException;

public class ConversationReceiveException extends IOException {
    public ConversationReceiveException(String message) {
        super(message);
    }
}
