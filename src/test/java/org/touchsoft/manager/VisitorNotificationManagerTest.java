package org.touchsoft.manager;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.service.VisitorNotificationService;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class VisitorNotificationManagerTest {
    @ParameterizedTest
    @ValueSource(strings = {"visitorFrom", "visitorTo", "visitorNotificationService"})
    void checkFieldIsNotNullAfterCreating(String name) throws NoSuchFieldException, IllegalAccessException {
        VisitorNotificationManager visitorNotificationManager = new VisitorNotificationManager(Mockito.mock(VisitorInstance.class),
                Mockito.mock(VisitorInstance.class), Mockito.mock(VisitorNotificationService.class));

        final Field field = visitorNotificationManager.getClass().getDeclaredField(name);
        field.setAccessible(true);

        assertNotNull(field.get(visitorNotificationManager), " value of field '" + name + "' is null after creating");
    }


    @Test
    void failedExecutingTestWhileGreeting() {
        VisitorInstance visitorInstance1 = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorInstance2 = Mockito.mock(VisitorInstance.class);

        VisitorNotificationService visitorNotificationService = Mockito.mock(VisitorNotificationService.class);
        Mockito.when(visitorNotificationService.greeting(visitorInstance1, visitorInstance2)).thenReturn(false);

        VisitorNotificationManager visitorNotificationManager = new VisitorNotificationManager(visitorInstance1, visitorInstance2,visitorNotificationService);
        visitorNotificationManager.run();

        assertFalse(visitorNotificationManager.isAlive(), "Manager was not stopped");
    }

    @Test
    void failedExecutingTestWhileNotify() {
        VisitorInstance visitorInstance1 = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorInstance2 = Mockito.mock(VisitorInstance.class);

        VisitorNotificationService visitorNotificationService = Mockito.mock(VisitorNotificationService.class);
        Mockito.when(visitorNotificationService.greeting(visitorInstance1, visitorInstance2)).thenReturn(true);
        Mockito.when(visitorNotificationService.notifyInterlocutor(visitorInstance1, visitorInstance2)).thenReturn(false);

        VisitorNotificationManager visitorNotificationManager = new VisitorNotificationManager(visitorInstance1, visitorInstance2,visitorNotificationService);
        visitorNotificationManager.run();

        assertFalse(visitorNotificationManager.isAlive(), "Manager was not stopped");

    }
}
