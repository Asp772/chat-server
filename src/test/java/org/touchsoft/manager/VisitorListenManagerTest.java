package org.touchsoft.manager;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.model.VisitorSocket;
import org.touchsoft.service.VisitorListenService;

import java.lang.reflect.Field;
import java.util.concurrent.ExecutorService;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class VisitorListenManagerTest {
    @ParameterizedTest
    @ValueSource(strings = {"visitorInstance", "visitorListenService"})
    void checkFieldIsNotNullAfterCreating(String name) throws NoSuchFieldException, IllegalAccessException {
        VisitorListenManager visitorListenManager = new VisitorListenManager(Mockito.mock(VisitorSocket.class), Mockito.mock(VisitorListenService.class));

        final Field field = visitorListenManager.getClass().getDeclaredField(name);
        field.setAccessible(true);

        assertNotNull(field.get(visitorListenManager), " value of field '" + name + "' is null after creating");
    }

    @Test
    void checkVisitorSocketReceiverAfterCreating() throws NoSuchFieldException, IllegalAccessException {
        VisitorListenManager visitorListenManager = new VisitorListenManager(Mockito.mock(VisitorSocket.class), Mockito.mock(VisitorListenService.class));

        final Field field = visitorListenManager.getClass().getDeclaredField("visitorInstance");
        field.setAccessible(true);
        VisitorInstance visitorInstance = (VisitorInstance) field.get(visitorListenManager);

        assertNotNull(visitorInstance.getVisitorSocket(), " value of visitorInstance's field 'visitorSocketReceiver' is not true after creating");
    }

    @Test
    void checkVisitorActionsAfterCreating() throws NoSuchFieldException, IllegalAccessException {
        VisitorListenManager visitorListenManager = new VisitorListenManager(Mockito.mock(VisitorSocket.class), Mockito.mock(VisitorListenService.class));

        final Field field = visitorListenManager.getClass().getDeclaredField("visitorInstance");
        field.setAccessible(true);
        VisitorInstance visitorInstance = (VisitorInstance) field.get(visitorListenManager);

        assertNotNull(visitorInstance.getVisitorActions(), " value of visitorInstance's field 'visitorActions' is not true after creating");
    }

    @Test
    void failedExecutingTestWhileAccept() {
        VisitorListenService visitorListenService = Mockito.mock(VisitorListenService.class);
        VisitorInstance visitorInstance = Mockito.mock(VisitorInstance.class);

        ExecutorService executorService = Mockito.mock(ExecutorService.class);
        Mockito.doReturn(false).when(visitorListenService).accept(visitorInstance);

        VisitorListenManager visitorListenManager = new VisitorListenManager(Mockito.mock(VisitorSocket.class), visitorListenService);
        visitorListenManager.run();

        assertFalse(visitorListenManager.isAlive(), "Manager was not stopped");
    }

    @Test
    void failedExecutingTestWhileListen() {
        VisitorListenService visitorListenService = Mockito.mock(VisitorListenService.class);
        VisitorInstance visitorInstance = Mockito.mock(VisitorInstance.class);

        ExecutorService executorService = Mockito.mock(ExecutorService.class);
        Mockito.doReturn(true).when(visitorListenService).accept(visitorInstance);
        Mockito.doReturn(false).when(visitorListenService).listen(visitorInstance);

        VisitorListenManager visitorListenManager = new VisitorListenManager(Mockito.mock(VisitorSocket.class), visitorListenService);
        visitorListenManager.run();

        assertFalse(visitorListenManager.isAlive(), "Manager was not stopped");
    }
}