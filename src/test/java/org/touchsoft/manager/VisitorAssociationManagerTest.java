package org.touchsoft.manager;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.touchsoft.exception.ConversationServiceRunningException;
import org.touchsoft.service.ConversationService;

import java.lang.reflect.Field;
import java.util.concurrent.ExecutorService;

import static org.junit.jupiter.api.Assertions.*;

public class VisitorAssociationManagerTest {


    @ParameterizedTest
    @ValueSource(strings = {"conversationService", "executorService"})
    void checkFieldIsNotNullAfterCreating(String name) throws NoSuchFieldException, IllegalAccessException {
        VisitorAssociationManager visitorAssociationManager = new VisitorAssociationManager(Mockito.mock(ConversationService.class), Mockito.mock(ExecutorService.class));

        final Field field = visitorAssociationManager.getClass().getDeclaredField(name);
        field.setAccessible(true);

        assertNotNull(field.get(visitorAssociationManager), " value of field '" + name + "' is null after creating");
    }

    @Test
    void checkFlagIsTrueAfterCreating() throws NoSuchFieldException, IllegalAccessException {
        VisitorAssociationManager visitorAssociationManager = new VisitorAssociationManager(Mockito.mock(ConversationService.class), Mockito.mock(ExecutorService.class));

        final Field field = visitorAssociationManager.getClass().getDeclaredField("isAlive");
        field.setAccessible(true);

        assertTrue((boolean) field.get(visitorAssociationManager), " value of field isAlive is not true after creating");
    }

    @Test
    void failedExecutingTest() throws ConversationServiceRunningException, NoSuchFieldException, IllegalAccessException {
        ConversationService conversationService = Mockito.mock(ConversationService.class);
        ExecutorService executorService = Mockito.mock(ExecutorService.class);
        Mockito.doThrow(new ConversationServiceRunningException("test exception message")).when(conversationService).createTalking(executorService);

        VisitorAssociationManager visitorAssociationManager = new VisitorAssociationManager(conversationService, executorService);
        visitorAssociationManager.run();

        final Field field = visitorAssociationManager.getClass().getDeclaredField("isAlive");
        field.setAccessible(true);

        assertFalse((boolean) field.get(visitorAssociationManager), " value of field isAlive is true after execution was stopped");
    }
}
