package org.touchsoft.storage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VisitorStorageTest {
    @Test
    void checkEquality() {
        assertSame(AwailableVisitorStorage.getInstance(), AwailableVisitorStorage.getInstance(), "Instances of storage are different");
    }

    @Test
    void checkInitialisation() {
        assertNotNull(AwailableVisitorStorage.getInstance().getAgentDeque(), "Collection was not initialized in constructor");
    }
    @Test
    void checkInitialisation2() {
        assertNotNull(AwailableVisitorStorage.getInstance().getClientDeque(), "Collection was not initialized in constructor");
    }
}
