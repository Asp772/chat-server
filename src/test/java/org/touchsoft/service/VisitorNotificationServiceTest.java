package org.touchsoft.service;

import jdk.nashorn.internal.ir.Block;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.touchsoft.exception.ConversationSendException;
import org.touchsoft.exception.SocketWrapperCloseException;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.model.VisitorSocket;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.service.impl.TalkingServiceImpl;
import org.touchsoft.service.impl.VisitorNotificationServiceImpl;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

class VisitorNotificationServiceTest {

    @Test
    void brokenGreetingTest() throws ConversationSendException {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doThrow(new ConversationSendException("test ex message")).when(talkingService).sendAction(any(VisitorInstance.class), any(Action.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertFalse(visitorNotificationService.greeting(visitorFrom, visitorTo), "False result was expected");
    }

    @Test
    void successGreetingTest() throws ConversationSendException {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doNothing().when(talkingService).sendAction(any(VisitorInstance.class), any(Action.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertTrue(visitorNotificationService.greeting(visitorFrom, visitorTo), "True result was expected");
    }

    @Test
    void nullGetInNotificationTest() {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doReturn(null).when(talkingService).getAction(any(VisitorInstance.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertTrue(visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo), "True result was expected");
    }

    @Test
    void notSupportedTypeOfActionInNotificationTest() {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doReturn(new Action(ActionType.REGISTER, null)).when(talkingService).getAction(any(VisitorInstance.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertTrue(visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo), "True result was expected");
    }

    @Test
    void conversationSendExceptionTest() throws ConversationSendException {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doThrow(new ConversationSendException("test ex message")).when(talkingService).sendAction(any(VisitorInstance.class), any(Action.class));
        Mockito.doReturn(new Action(ActionType.SEND_MESSAGE, null)).when(talkingService).getAction(any(VisitorInstance.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertFalse(visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo), "False result was expected");
    }


    @Test
    void interlocutorBrokenTest() {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doReturn(new Action(ActionType.INTERLOCUTOR_BROKEN, null)).when(talkingService).getAction(any(VisitorInstance.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertFalse(visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo), "False result was expected");
    }

    @Test
    void forgetInterlocutorTest() {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doReturn(new Action(ActionType.FORGET_INTERLOCUTOR, null)).when(talkingService).getAction(any(VisitorInstance.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertFalse(visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo), "False result was expected");
    }

    @Test
    void forgetInterlocutorTest2() throws ConversationSendException {
        TalkingService talkingService = Mockito.mock(TalkingServiceImpl.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);
        Action forgetAction = new Action(ActionType.FORGET_INTERLOCUTOR, null);
        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);
        BlockingQueue<Action> actions = new ArrayBlockingQueue<>(1);
        Mockito.when(visitorTo.getVisitorActions()).thenReturn(actions);
        Mockito.doReturn(forgetAction).when(talkingService).getAction(any(VisitorInstance.class));

        Mockito.doAnswer(invocation -> {
            Object testActionInService = invocation.getArguments()[1];
            assertSame(testActionInService, forgetAction, "Action was not passed to service correctly");
            return null;
        }).when(talkingService).sendAction(any(VisitorInstance.class), any(Action.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo);
    }

    @Test
    void forgetInterlocutorTest3() {
        TalkingService talkingService = Mockito.mock(TalkingServiceImpl.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);
        Action forgetAction = new Action(ActionType.FORGET_INTERLOCUTOR, null);
        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);
        BlockingQueue<Action> actions = new ArrayBlockingQueue<>(1);
        Mockito.when(visitorTo.getVisitorActions()).thenReturn(actions);
        Mockito.doReturn(forgetAction).when(talkingService).getAction(any(VisitorInstance.class));
        Mockito.doCallRealMethod().when(talkingService).addAction(any(VisitorInstance.class), any(Action.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo);
        assertSame(forgetAction.getType(), visitorTo.getVisitorActions().poll().getType(), "Action was not changed to FORGOTTEN type");
    }

    @Test
    void interlocutorForgottenTest() {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doReturn(new Action(ActionType.FORGOTTEN, null)).when(talkingService).getAction(any(VisitorInstance.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertFalse(visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo), "False result was expected");
    }

    @Test
    void forgottenInterlocutorCloseTest() throws SocketWrapperCloseException {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = new VisitorInstance();

        VisitorSocket visitorSocketReceiver = Mockito.mock(VisitorSocket.class);
        VisitorSocket visitorSocketSender = Mockito.mock(VisitorSocket.class);

        Mockito.doNothing().when(visitorSocketReceiver).closeSocket();
        Mockito.doNothing().when(visitorSocketSender).closeSocket();
        visitorTo.setVisitorSocket(visitorSocketReceiver);

        Mockito.doReturn(new Action(ActionType.FORGOTTEN, null)).when(talkingService).getAction(any(VisitorInstance.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo);
        assertFalse(visitorTo.isConnected(), "False result was expected");
    }


    @Test
    void conversationSendExceptionCloseSocketTest() throws ConversationSendException, SocketWrapperCloseException {
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = new VisitorInstance();

        VisitorSocket visitorSocketReceiver = Mockito.mock(VisitorSocket.class);
        VisitorSocket visitorSocketSender = Mockito.mock(VisitorSocket.class);

        Mockito.doNothing().when(visitorSocketReceiver).closeSocket();
        Mockito.doNothing().when(visitorSocketSender).closeSocket();
        visitorTo.setVisitorSocket(visitorSocketReceiver);

        Mockito.doReturn(new Action(ActionType.SEND_MESSAGE, null)).when(talkingService).getAction(any(VisitorInstance.class));
        Mockito.doThrow(new ConversationSendException("test ex message")).when(talkingService).sendAction(any(VisitorInstance.class), any(Action.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo);
        assertFalse(visitorTo.isConnected(), "False result was expected");
    }

    @Test
    void successNotificationTest() throws ConversationSendException {
        TalkingService talkingService = Mockito.mock(TalkingServiceImpl.class);
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Action testAction = new Action(ActionType.SEND_MESSAGE, new MessagePayload("test message"));
        Mockito.doNothing().when(talkingService).sendAction(any(VisitorInstance.class), any(Action.class));
        Mockito.doReturn(testAction).when(talkingService).getAction(any(VisitorInstance.class));

        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        assertTrue(visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo), "true result was expected");
    }

    @Test
    void successNotificationTest2() throws ConversationSendException {
        Action testAction = new Action(ActionType.SEND_MESSAGE, new MessagePayload("test message"));

        TalkingService talkingService = Mockito.mock(TalkingServiceImpl.class);
        Mockito.doAnswer(invocation -> {
            Object testActionInService = invocation.getArguments()[1];
            assertSame(testActionInService, testAction, "Action was not passed to service correctly");
            return null;
        }).when(talkingService).sendAction(any(VisitorInstance.class), any(Action.class));

        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);

        VisitorInstance visitorFrom = Mockito.mock(VisitorInstance.class);
        VisitorInstance visitorTo = Mockito.mock(VisitorInstance.class);

        Mockito.doReturn(testAction).when(talkingService).getAction(any(VisitorInstance.class));
        VisitorNotificationServiceImpl visitorNotificationService = new VisitorNotificationServiceImpl(talkingService, visitorRegistrationService);
        visitorNotificationService.notifyInterlocutor(visitorFrom, visitorTo);
    }
}
