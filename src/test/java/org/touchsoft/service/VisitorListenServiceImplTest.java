package org.touchsoft.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.touchsoft.exception.ConversationReceiveException;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.service.impl.VisitorListenServiceImpl;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class VisitorListenServiceImplTest {

    @ParameterizedTest
    @ValueSource(strings = {"talkingService", "visitorRegistrationService"})
    void checkFieldIsNotNullAfterCreating(String name) throws NoSuchFieldException, IllegalAccessException {
        VisitorListenServiceImpl visitorListenService = new VisitorListenServiceImpl(Mockito.mock(TalkingService.class),
                Mockito.mock(VisitorRegistrationService.class));

        final Field field = visitorListenService.getClass().getDeclaredField(name);
        field.setAccessible(true);

        assertNotNull(field.get(visitorListenService), " value of field '" + name + "' is null after creating");
    }

    @Test
    void listenTestSuccess() throws ConversationReceiveException {
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorInstance visitorInstance = Mockito.mock(VisitorInstance.class);
        Action action = new Action(ActionType.LEAVE, null);

        Mockito.when(talkingService.receiveAction(visitorInstance)).thenReturn(action);

        VisitorListenServiceImpl visitorListenService = new VisitorListenServiceImpl(talkingService, visitorRegistrationService);

        assertFalse (visitorListenService.listen(visitorInstance), "'listen' method does not produce false result while it was expected");
    }

    @Test
    void listenTestException() throws ConversationReceiveException {
        VisitorRegistrationService visitorRegistrationService = Mockito.mock(VisitorRegistrationService.class);
        TalkingService talkingService = Mockito.mock(TalkingService.class);
        VisitorInstance visitorInstance = Mockito.mock(VisitorInstance.class);

        Mockito.when(talkingService.receiveAction(visitorInstance)).thenThrow(new ConversationReceiveException("test ex message"));

        VisitorListenServiceImpl visitorListenService = new VisitorListenServiceImpl(talkingService, visitorRegistrationService);

        assertFalse (visitorListenService.listen(visitorInstance), "'listen' method does not produce false result while it was expected");
    }

}
