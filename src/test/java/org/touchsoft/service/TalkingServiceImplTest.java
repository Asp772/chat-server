package org.touchsoft.service;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.touchsoft.exception.*;
import org.touchsoft.model.VisitorInstance;
import org.touchsoft.model.VisitorSocket;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.ActionType;
import org.touchsoft.model.action.payload.impl.MessagePayload;
import org.touchsoft.service.impl.TalkingServiceImpl;
import org.touchsoft.service.impl.MessageTransferServiceImpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TalkingServiceImplTest {
    private static VisitorInstance visitorInstance;
    private static Action testAction;
    private static MessageTransferService messageTransferService;

    @BeforeEach
    void prepare() {
        visitorInstance = new VisitorInstance();
        testAction = new Action(ActionType.SEND_MESSAGE, new MessagePayload("test messsage"));
        messageTransferService = Mockito.mock(MessageTransferService.class);
    }

    @Test
    void addActionTest() {
        visitorInstance.setVisitorActions(new ArrayBlockingQueue<>(1));

        TalkingServiceImpl talkingService = new TalkingServiceImpl(new MessageTransferServiceImpl());
        talkingService.addAction(visitorInstance, testAction);

        assertEquals(testAction, visitorInstance.getVisitorActions().poll(), "Action was not added correctly");
    }

    @Test
    void getActionTest() throws InterruptedException {
        BlockingQueue<Action> tempQueue = new ArrayBlockingQueue<>(1);
        tempQueue.put(testAction);
        VisitorInstance visitorInstance = Mockito.mock(VisitorInstance.class);
        Mockito.when(visitorInstance.getVisitorActions()).thenReturn(tempQueue);
        TalkingServiceImpl talkingService = new TalkingServiceImpl(Mockito.mock(MessageTransferService.class));

        assertEquals(testAction, talkingService.getAction(visitorInstance), "Action was not received correctly");
    }

    @Test
    void receiveActionTestFailed() throws MessageTransferReceiveException {
        VisitorSocket visitorSocket = Mockito.mock(VisitorSocket.class);
        visitorInstance.setVisitorSocket(visitorSocket);
        MessageTransferService messageTransferService = Mockito.mock(MessageTransferService.class);
        Mockito.when(messageTransferService.receive(visitorSocket)).thenThrow(new MessageTransferReceiveException("test ex message"));

        TalkingServiceImpl talkingService = new TalkingServiceImpl(messageTransferService);

        Assertions.assertThrows(ConversationReceiveException.class, () -> {
            talkingService.receiveAction(visitorInstance);
        }, "Action received while it was not expected");
    }

    @Test
    void receiveActionTestSuccess() throws MessageTransferReceiveException, ConversationReceiveException {
        VisitorSocket visitorSocket = Mockito.mock(VisitorSocket.class);
        visitorInstance.setVisitorSocket(visitorSocket);
        MessageTransferService messageTransferService = Mockito.mock(MessageTransferService.class);
        Mockito.when(messageTransferService.receive(visitorSocket)).thenReturn(testAction);
        TalkingServiceImpl talkingService = new TalkingServiceImpl(messageTransferService);

        assertEquals(talkingService.receiveAction(visitorInstance), testAction, "Action was not received correctly");

    }

    @Test
    void sendActionTestFailed() throws MessageTransferSendException {
        Action testAction = new Action(ActionType.SEND_MESSAGE, new MessagePayload("test messsage"));
        VisitorSocket visitorSocket = Mockito.mock(VisitorSocket.class);
        visitorInstance.setVisitorSocket(visitorSocket);
        MessageTransferService messageTransferService = Mockito.mock(MessageTransferService.class);
        Mockito.when(messageTransferService.send(visitorSocket, testAction)).thenThrow(new MessageTransferSendException("test ex message"));

        TalkingServiceImpl talkingService = new TalkingServiceImpl(messageTransferService);

        Assertions.assertThrows(ConversationSendException.class, () -> {
            talkingService.sendAction(visitorInstance, testAction);
        }, "Action send while it was not expected");
    }

    @Test
    void sendActionTestSuccess() throws IOException, ClassNotFoundException {
        VisitorSocket visitorSocket = Mockito.mock(VisitorSocket.class);
        ByteArrayOutputStream response = new ByteArrayOutputStream();
        Mockito.when(visitorSocket.getOutputStream()).thenReturn(response);
        visitorInstance.setVisitorSocket(visitorSocket);
        MessageTransferService messageTransferService = new MessageTransferServiceImpl();

        TalkingServiceImpl talkingService = new TalkingServiceImpl(messageTransferService);
        talkingService.sendAction(visitorInstance, testAction);

        byte[] bytes = response.toByteArray();

        ObjectInputStream objectInput = new ObjectInputStream(new ByteArrayInputStream(bytes));
        Action actionToReceive = (Action) objectInput.readObject();

        assertEquals(testAction, actionToReceive, "Request desnot equal to response");
    }

    @Test
    void errorOccuredTest() throws NoSuchFieldException, IllegalAccessException {
        visitorInstance.setVisitorActions(new ArrayBlockingQueue<>(1));

        TalkingServiceImpl talkingService = new TalkingServiceImpl(new MessageTransferServiceImpl());
        talkingService.errorOccured(visitorInstance);

        final Field field = talkingService.getClass().getDeclaredField("BROKEN_INTERLOCUTOR");
        field.setAccessible(true);

        assertEquals(field.get(talkingService), visitorInstance.getVisitorActions().poll(),
                "Action about broken connection was not added correctly");
    }

}
