package org.touchsoft.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.touchsoft.exception.SocketWrapperCreateException;
import org.touchsoft.exception.SocketGetInputStreamException;
import org.touchsoft.exception.SocketGetOutputStreamException;
import org.touchsoft.exception.SocketWrapperCloseException;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.assertSame;

public class VisitorSocketTest {

    @Test
    void visitorSocketGetInputStreamFaied() {
        Assertions.assertThrows(SocketGetInputStreamException.class, () -> {
            VisitorSocket visitorSocket = new VisitorSocket(new Socket() {
                @Override
                public InputStream getInputStream() throws IOException {
                    throw new IOException("exc message");
                }
            });
            visitorSocket.getInputStream();
        });
    }

    @Test
    void visitorSocketGetOutputStreamFaied() {
        Assertions.assertThrows(SocketGetOutputStreamException.class, () -> {
            Socket socket = Mockito.mock(Socket.class);
            Mockito.when(socket.getOutputStream()).thenThrow(new IOException("test exception message"));

            VisitorSocket visitorSocket = new VisitorSocket(socket);

            visitorSocket.getOutputStream();
        });
    }

    @Test
    void visitorSocketClosingSocketFailed() {
        Assertions.assertThrows(SocketWrapperCloseException.class, () -> {
            Socket socket = Mockito.mock(Socket.class);
            Mockito.doThrow(new IOException("test exception message")).when(socket).close();

            VisitorSocket visitorSocket = new VisitorSocket(socket);

            visitorSocket.closeSocket();
        });
    }

    @Test
    void visitorSocketGetInetAddress() throws UnknownHostException {
        InetAddress inetAddress = InetAddress.getByName("127.0.0.1");

        Socket socket = Mockito.mock(Socket.class);
        Mockito.when(socket.getInetAddress()).thenReturn(inetAddress);

        VisitorSocket visitorSocket = new VisitorSocket(socket);

        assertSame(visitorSocket.getInetAddress(), inetAddress, "getInetAddress of VisitorSocket.class returns unexpected data");
    }

    @Test
    void visitorSocketGetLocalAddress() throws UnknownHostException {
        InetAddress inetAddress = InetAddress.getByName("127.0.0.1");

        Socket socket = Mockito.mock(Socket.class);
        Mockito.when(socket.getLocalAddress()).thenReturn(inetAddress);

        VisitorSocket visitorSocket = new VisitorSocket(socket);

        assertSame(visitorSocket.getLocalAddress(), inetAddress, "getLocalAddress of VisitorSocket.class returns unexpected data");
    }

    @Test
    void visitorSocketCreationFailed() {
        Assertions.assertThrows(SocketWrapperCreateException.class, () -> {
            new VisitorSocket("abc", 1);
        });
    }
}
