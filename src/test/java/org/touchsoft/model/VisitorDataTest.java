package org.touchsoft.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.touchsoft.model.action.payload.impl.Role;
import org.touchsoft.model.action.payload.impl.VisitorData;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class VisitorDataTest {
    final private String VISITOR_NAME = "test name";
    final private Role CLIENT_ROLE = Role.ROLE_CLIENT;

    private VisitorData visitorData;

    @BeforeEach
    void setUp() {
        this.visitorData = new VisitorData(VISITOR_NAME, CLIENT_ROLE);
    }

    @Test
    void getName() throws NoSuchFieldException, IllegalAccessException {
        final Field field = this.visitorData.getClass().getDeclaredField("name");
        field.setAccessible(true);

        assertSame(field.get(this.visitorData), VISITOR_NAME, "value of field 'name' did not match with expected");
    }

    @Test
    void getNameByGetter() throws NoSuchFieldException, IllegalAccessException {
        final Field field = this.visitorData.getClass().getDeclaredField("name");
        field.setAccessible(true);

        assertSame(field.get(this.visitorData), this.visitorData.getName(), "value of field 'name' did not match with expected");
    }

    @Test
    void geRole() throws NoSuchFieldException, IllegalAccessException {
        final Field field = this.visitorData.getClass().getDeclaredField("role");
        field.setAccessible(true);

        assertSame(field.get(this.visitorData), CLIENT_ROLE, "value of field 'role' did not match with expected");
    }

    @Test
    void geRoleGetter() throws NoSuchFieldException, IllegalAccessException {
        final Field field = this.visitorData.getClass().getDeclaredField("role");
        field.setAccessible(true);

        assertSame(field.get(this.visitorData).getClass(), this.visitorData.getRole().getClass(), "type of field 'role' did not match with expected");
    }
}
