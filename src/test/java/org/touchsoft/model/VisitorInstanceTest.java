package org.touchsoft.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.touchsoft.exception.ConversationServiceRunningException;
import org.touchsoft.exception.SocketWrapperCloseException;
import org.touchsoft.model.action.Action;
import org.touchsoft.model.action.payload.impl.Role;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class VisitorInstanceTest {

    @Test
    void isConnectedAfterClosing() {
        VisitorInstance visitorInstance = Mockito.mock(VisitorInstance.class);
        visitorInstance.close();

        assertFalse(visitorInstance.isConnected(), "boolean 'isConnected' is not 'false' after instance closing");
    }

    private static Stream<Arguments> parametersForClosing() {
        return Stream.of(Arguments.of("role", "setRole", "getRole", Role.class, Role.ROLE_CLIENT),
                Arguments.of("role", "setRole", "getRole", Role.class, Role.ROLE_AGENT),
                Arguments.of("visitorActions", "setVisitorActions", "getVisitorActions",
                        BlockingQueue.class, new ArrayBlockingQueue<Action>(1)),
                Arguments.of("visitorSocketSender", "setSocketWrapperSender", "getSocketWrapperSender",
                        VisitorSocket.class, Mockito.mock(VisitorSocket.class)),
                Arguments.of("name", "setName", "getName", String.class, "Test name"),
                Arguments.of("visitorSocketReceiver", "setSocketWrapperReceiver", "getSocketWrapperReceiver",
                        VisitorSocket.class, Mockito.mock(VisitorSocket.class))
        );
    }

    @Test
    void isClosedSocket() throws SocketWrapperCloseException {
        VisitorInstance visitorInstance = new VisitorInstance();
        VisitorSocket visitorSocketReceiver = Mockito.mock(VisitorSocket.class);
        VisitorSocket visitorSocketSender = Mockito.mock(VisitorSocket.class);
        Mockito.doThrow(new SocketWrapperCloseException("test exception message")).when(visitorSocketReceiver).closeSocket();
        Mockito.doNothing().when(visitorSocketSender).closeSocket();
        visitorInstance.setVisitorSocket(visitorSocketReceiver);

        visitorInstance.close();
    }

    @Test
    void isClosedReceiver() throws SocketWrapperCloseException {
        VisitorInstance visitorInstance = new VisitorInstance();
        VisitorSocket visitorSocketReceiver = Mockito.mock(VisitorSocket.class);
        VisitorSocket visitorSocketSender = Mockito.mock(VisitorSocket.class);
        Mockito.doThrow(new SocketWrapperCloseException("test exception message")).when(visitorSocketSender).closeSocket();
        Mockito.doNothing().when(visitorSocketReceiver).closeSocket();
        visitorInstance.setVisitorSocket(visitorSocketReceiver);

        visitorInstance.close();
    }


    @Test
    void isConnectedAfterCreating() throws NoSuchFieldException, IllegalAccessException {
        VisitorInstance visitorInstance = new VisitorInstance();

        final Field field = visitorInstance.getClass().getDeclaredField("isConnected");
        field.setAccessible(true);

        assertTrue(((AtomicBoolean) field.get(visitorInstance)).get(), " value of boolean 'isConnected' is not true after creating instance");
    }

    @Test
    void isConnectedAfterSetting() throws NoSuchFieldException, IllegalAccessException {
        VisitorInstance visitorInstance = new VisitorInstance();
        visitorInstance.setConnected(false);

        final Field field = visitorInstance.getClass().getDeclaredField("isConnected");
        field.setAccessible(true);

        assertEquals(((AtomicBoolean) field.get(visitorInstance)).get(), visitorInstance.isConnected(), " value of boolean 'isConnected' is not equal to get method");
    }

    @ParameterizedTest
    @ValueSource(strings = {"role", "name", "visitorSocket", "visitorActions"})
    void checkFieldIsNullAfterCreating(String name) throws NoSuchFieldException, IllegalAccessException {
        VisitorInstance visitorInstance = new VisitorInstance();

        final Field field = visitorInstance.getClass().getDeclaredField(name);
        field.setAccessible(true);

        assertNull(field.get(visitorInstance), " value of field '" + name + "' is not null after creating");
    }

    @ParameterizedTest
    @MethodSource("parametersForSettersProvider")
    void testSetters(String fieldName, String methodName, Class paramType, Object param) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        VisitorInstance visitorInstance = new VisitorInstance();

        Method method = visitorInstance.getClass().getMethod(methodName, paramType);
        method.invoke(visitorInstance, param);

        final Field field = visitorInstance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);

        assertEquals(field.get(visitorInstance), param, " value of field '" + fieldName + "' is not equal to setter method return");
    }

    private static Stream<Arguments> parametersForSettersProvider() {
        return Stream.of(Arguments.of("role", "setRole", Role.class, Role.ROLE_CLIENT),
                Arguments.of("role", "setRole", Role.class, Role.ROLE_AGENT),
                Arguments.of("visitorActions", "setVisitorActions", BlockingQueue.class,
                        new ArrayBlockingQueue<Action>(1)),
                Arguments.of("visitorSocket", "setVisitorSocket", VisitorSocket.class,
                        Mockito.mock(VisitorSocket.class)),
                Arguments.of("name", "setName", String.class, "Test name")
        );
    }

    @ParameterizedTest
    @MethodSource("parametersForGettersProvider")
    void testGetters(String fieldName, String setterName, String getterName, Class paramType, Object param) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        VisitorInstance visitorInstance = new VisitorInstance();

        Method setter = visitorInstance.getClass().getMethod(setterName, paramType);
        setter.invoke(visitorInstance, param);

        final Field field = visitorInstance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);

        Method getter = visitorInstance.getClass().getMethod(getterName);

        Object result = getter.invoke(visitorInstance);
        assertEquals(field.get(visitorInstance), result, " value of field '" + fieldName + "' is not equal to getter method return");
    }

    private static Stream<Arguments> parametersForGettersProvider() {
        return Stream.of(Arguments.of("role", "setRole", "getRole", Role.class, Role.ROLE_CLIENT),
                Arguments.of("role", "setRole", "getRole", Role.class, Role.ROLE_AGENT),
                Arguments.of("visitorActions", "setVisitorActions", "getVisitorActions",
                        BlockingQueue.class, new ArrayBlockingQueue<Action>(1)),
                Arguments.of("visitorSocket", "setVisitorSocket", "getVisitorSocket",
                        VisitorSocket.class, Mockito.mock(VisitorSocket.class)),
                Arguments.of("name", "setName", "getName", String.class, "Test name")
        );
    }


}
